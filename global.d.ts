declare namespace GlobalMixins {
    export interface DisplayObject {
		update(deltaTime: number): void;
		isActive: boolean;
	}
    export interface Container {
		update(deltaTime: number): void;
	}
}