import { Application, DisplayObject, Point, Sprite } from "pixi.js";
import { IWertApplicationConfig, Size, ViewportSize } from "../Types";
import { InputManager } from "./input/InputManager";
import { Stage } from "./Stage";

const DefaultViewportSize: ViewportSize = {
    max: {
        width: 1920,
        height: 1080
    },
    min: {
        width: 960,
        height: 544
    },
    current: {
        width: 0,
        height: 0
    }
};

export class WertGameInstance
{
    private _pixiApplicationInstance: Application;
    private _currentStage: Stage | null = null;
    public inputManager: InputManager = new InputManager();
    public viewportSize: ViewportSize = new ViewportSize();
    private _isSoundPlayed: boolean = false;
    private _isMuted: boolean = true;
    private _isDebug: boolean = false;

    constructor(pixiApplication: Application, config: IWertApplicationConfig)
    {
        this._pixiApplicationInstance = pixiApplication;
        if (config.viewportSize)
        {
            Object.assign(this.viewportSize, config.viewportSize);
        }
        else
        {
            Object.assign(this.viewportSize, DefaultViewportSize);
        }
        window.addEventListener("resize", this.resizeNative.bind(this));
        this.resize(new Size(window.innerWidth, window.innerHeight));

        this.switchStage(config.initialStage);
    }

    switchStage(newStage: Stage) {
        if (this._currentStage != null) {
            this._pixiApplicationInstance?.stage.removeChild(this._currentStage);
            this._currentStage = null;
        }
        this._currentStage = newStage;
        newStage.create();
        newStage.resize(this.viewportSize.min);
        this._pixiApplicationInstance?.stage.addChild(this._currentStage);
    }
    

    resizeNative(event: UIEvent)
    {
        let newSize: Size = new Size(
            window.innerWidth,
            window.innerHeight
        );
        this.resize(newSize);
    }

    resize(newSize: Size) {
        let currentAspectRatio = newSize.width / newSize.height;
        let targetScale = 1.0;
        let targetAspectRatio = this.viewportSize.min.width / this.viewportSize.min.height;
        let w, h;
        if (currentAspectRatio > targetAspectRatio)
        {
            h = newSize.height;
            w = Math.floor(h * targetAspectRatio);
            
            targetScale = newSize.height / this.viewportSize.current.height;
        }
        else
        {
            w = newSize.width;
            h =  Math.floor(w / targetAspectRatio);
            
            targetScale = newSize.width / this.viewportSize.current.width;
        }

        let view = this._pixiApplicationInstance?.renderer.view as HTMLCanvasElement;
        if (view && view.style)
        {
            view.style.height = h + 'px';
            view.style.width = w + 'px';
            view.style.left = Math.floor(( newSize.width - w) / 2) + 'px';
            view.style.top = Math.floor(( newSize.height - h) / 2) + 'px';
        }   
        this.viewportSize.current = newSize;
    }

    
    public update(deltaTime: number) {
        this.inputManager.update();
        if (this._currentStage != null) 
        {
            this._currentStage.update( this._pixiApplicationInstance?.ticker.elapsedMS / 1000);
        }
    };

    public get isSoundPlayed() {
        return this._isSoundPlayed;
    }

    public get isMuted() {
        return this._isMuted;
    }

    public get isDebug()
    {
        return this._isDebug;
    }

    public static getRootStage(object: DisplayObject): Stage | null
    {
        while (object != null && !(object instanceof Stage))
        {
            object = object.parent;
        }
        return object;
    }

    public get currentStage(): Stage | null
    {
        return this._currentStage;
    }
}