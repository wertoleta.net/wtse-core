export enum ActionState {
    None = 0,
    Fired = 1,
    Active = 2,
    Ends = 3
};

export class Action {
    private _state: ActionState = ActionState.None;
    private _nextState: ActionState = ActionState.None;
    private _activationsCount: number =  0;

    constructor() {
    }

    public update() 
    {
        switch (this._state)
        {
        case ActionState.Fired:
            this._state = ActionState.Active;
            break;
        case ActionState.Ends:
            this._state = ActionState.None;
            break;
        }
        if (this._nextState !=ActionState.None)
        {
            this._state = this._nextState;
            this._nextState = ActionState.None;
        }
    }
    
    public activate()
    {
        if (this._activationsCount == 0)
        {
            this._nextState = ActionState.Fired;
        }
        this._activationsCount++;
    }
    
    public deactivate() 
    {
        this._activationsCount = Math.max(0, this._activationsCount - 1);
        if (this._activationsCount == 0)
        {
            this._nextState = ActionState.Ends;
        }
    }
    
    public get isFired()
    {
        return this._state == ActionState.Fired;
    }
    
    public get isActive()
    {
        return this._state != ActionState.None;
    }
    
    public get isEnds()
    {
        return this._state == ActionState.Ends;
    }
};