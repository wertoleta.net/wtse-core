import { Point } from "pixi.js";

export class Mouse {
    private _x: number = 0; 
    private _y: number = 0;
    private _isPressed: boolean = false;

    public update() 
    {
        this._isPressed = false;
    }

    public press() 
    {
        this._isPressed = true;
    }

    public get isPressed()
    {
        return this._isPressed;
    }

    public get x()
    {
        return this._x;
    }

    public set x(x: number)
    {
        this._x = x;
    }

    public get y()
    {
        return this._y;
    }

    public set y(y: number)
    {
        this._y = y;
    }
}