import { Action } from "./Action";

export class InputManager {
    private _registeredActions: Map<string, Action> = new Map<string, Action>();
    private _keyBindedActions: Map<string, Action> = new Map<string, Action>();
    private _keysPressedState: Map<string, boolean> = new Map<string, boolean>();
    
    public update()
    {
        this._registeredActions.forEach(action => {
            action.update();
        });
    }

    public getAction(actionName: string) : Action | undefined
    {
        return this._registeredActions.get(actionName);
    }

    public registerKeyAction(actionName: string, keyCode: string)
    { 
        let action = this._registeredActions.get(actionName);
        if (!action)
        {
            action = new Action();
            this._registeredActions.set(actionName, action);
        }
        this._keyBindedActions.set(keyCode, action);
    }

    public keyDown(event: KeyboardEvent)
    {
        if (this._keysPressedState.get(event.code))
        {
            return;
        }
        this._keysPressedState.set(event.code, true);
        
        let action = this._keyBindedActions.get(event.code);
        if (action)
        {
            action.activate();
        }
    }
    
    public keyUp(event: KeyboardEvent)
    {
        this._keysPressedState.set(event.code, false);
        
        let action = this._keyBindedActions.get(event.code);
        if (action)
        {
            action.deactivate();
        }
    }
}