import { Container, Sprite, Texture } from "pixi.js";
import { WertGameInstance } from "../WertGameInstance";

export class Button extends Container
{
    private _callback: Function;
	private _sprite: Sprite;
	private _baseScale: number = 1.0;
	public isActive: boolean = true;

	constructor(texture: Texture, callback: Function)
	{
		super();

		this._callback = callback;
	
		this._sprite = new Sprite(texture);
		this._sprite.anchor.set(0.5);
		this.addChild(this._sprite);
	}

	update(deltaTime: number) {
		super.update(deltaTime);

		let targetScale = this._baseScale;
		if (this.isActive)
		{
			let mouse = WertGameInstance.getRootStage(this)?.mouse;
			if (mouse != null && this.getBounds().contains(mouse.x, mouse.y))
			{
				targetScale = this._baseScale * 1.2;
				if (mouse.isPressed)
				{
					this._callback();
				}
			}
		}
		this.scale.set(targetScale);
	}
} 