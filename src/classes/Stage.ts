import { Container, InteractionData, InteractionEvent, Point } from "pixi.js";
import { Size, ViewportSize } from "../Types";
import { Mouse } from "./input/Mouse";

export class Stage extends Container
{
    private _mouse: Mouse = new Mouse();

    constructor(){
        super();

        this.eventMode = "dynamic";

        this.on('mousemove', this.onMove);
        this.on('touchmove', this.onMove);
        this.on('mousedown', this.onDown);
        this.on('touchstart', this.onDown);
    }

    update(deltaTime: number): void {
        super.update(deltaTime);
        this._mouse.update();
    }

    create(){};

    resize(viewportSize:Size){};

    pointFromGlobalToLocal(global: Point) {
        return new Point(-this.x + global.x / this.scale.x, -this.y + global.y / this.scale.y);
    }

    onDown(event: InteractionEvent) {	
        let viewport = this.pointFromGlobalToLocal(event.data.global);
        this._mouse.x = viewport.x; 
        this._mouse.y = viewport.y; 
        this._mouse.press();
    }
    
    onUp(event: InteractionEvent) {	
    }
    
    onMove(event: InteractionEvent) {	
        let viewport = this.pointFromGlobalToLocal(event.data.global);
        this._mouse.x = viewport.x;
        this._mouse.y = viewport.y;
    }

    public get mouse() {
        return this._mouse;
    }
}