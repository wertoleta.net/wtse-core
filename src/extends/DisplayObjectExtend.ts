import { DisplayObject } from "@pixi/display";


export function DisplayObjectExtend(displayObject: typeof DisplayObject) {

	if (!displayObject)
		throw new Error("Cant't find DisplayObject in package!");

	/**
	 * @mixes
	 * MIXIN FROM pixiv5-tiled
	 * Get child by path
	 */

	Object.assign(displayObject.prototype,
		{
			isActive: true,
            update:  function(deltaTime: number){}
		});
}
