import { DisplayObject, Container } from "@pixi/display";


export function ContainerExtend(container: typeof Container) {

	if (!container)
		throw new Error("Cant't find Container in package!");

	/**
	 * @mixes
	 * MIXIN FROM pixiv5-tiled
	 * Get child by path
	 */

	Object.assign(container.prototype,
		{
            update:  function(deltaTime: number){
				const _this = <Container> <any> this;
                for (var childIndex = 0; childIndex < _this.children.length; childIndex++)
                { 
					var child = (_this.getChildAt(childIndex) as any);
					if (child.isActive)
					{
                    	child.update(deltaTime);
					}
                }
            }
		});
}
