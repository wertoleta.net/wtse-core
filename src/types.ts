import { IApplicationOptions} from 'pixi.js';
import { Stage } from './classes/Stage';

export class Size {
    width: number = 0;
    height: number = 0;

    constructor(width: number = 0, height: number = 0)
    {
        this.width = width;
        this.height = height;
    }
}

export class ViewportSize {
    max: Size = new Size();
    min: Size = new Size();
    current: Size = new Size();
}

export interface IWertApplicationConfig {
    viewportSize?: ViewportSize,
    pixiConfig?: IApplicationOptions,
    initialStage: Stage
};