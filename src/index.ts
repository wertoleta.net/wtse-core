import { WertGameInstance } from "./classes/WertGameInstance";
import { Application, DisplayObject, Container } from 'pixi.js';
import { IApplicationOptions} from 'pixi.js';
import { IWertApplicationConfig as IWertApplicationConfigReal, ViewportSize } from "./Types";
import { Action } from "./classes/input/Action";
import { DisplayObjectExtend } from "./extends/DisplayObjectExtend";
import { ContainerExtend } from "./extends/ContainerExtend";

export function start(config: IWertApplicationConfigReal, parent: HTMLElement): WertGameInstance
{
    DisplayObjectExtend(DisplayObject);
    ContainerExtend(Container);

    let pixiApplication = new Application<HTMLCanvasElement>(config.pixiConfig);
    let newGameInstance = new WertGameInstance(pixiApplication, config);

    parent.appendChild(pixiApplication.view);

    pixiApplication.ticker.add(newGameInstance.update, newGameInstance);

    window.addEventListener("keydown", newGameInstance.inputManager.keyDown.bind(newGameInstance.inputManager));
    window.addEventListener("keyup", newGameInstance.inputManager.keyUp.bind(newGameInstance.inputManager));

    return newGameInstance;
}

export { WertGameInstance } from './classes/WertGameInstance'
export { Action } from './classes/input/Action';
export { InputManager } from './classes/input/InputManager';
export { Mouse } from './classes/input/Mouse';
export { ViewportSize} from './Types';
export { Stage } from './classes/Stage';
export * from './Types';
export type IWertApplicationConfig = IWertApplicationConfigReal;