const {main} = require('@pixi-build-tools/rollup-configurator/main');

module.exports = () => main(
	{
		external: [
			"pixi.js"	
		],
		globals: {
			"@pixi/sprite-animated": "PIXI"
		}
	});